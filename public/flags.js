const errorFlags = {
  badRequest:  { type: 'error', title: 'Invalid payload. Please review the payload (e.g. mandatory fields).' },
  notFound: { type: 'error', title: 'Either client, resource or specified scope does not exist. Please review the payload.' },
  serverError: { type: 'error', title: 'There was something wrong processing the request' }
};

const successFlags = {
  installSuccess: { type: 'success', title: 'Client grants created/updated.' }
};
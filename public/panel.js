require(["opshub/services", "opshub/util", "public/flags", "public/queries"], function (services, util, flags, queries) {

	const panelPresenter = (() => {

		const service = services.selectedService();

		const loadingState = (loadingElement, buttonElement, isLoading) => {
			if (isLoading) {
				const panel = $("#client-grant-panel");
				panel.find("#validation-result").html("");
				panel.find("#install-result").html("");
			}
			loadingElement.show().spin(isLoading);
			buttonElement.prop("disabled", isLoading);
		};

		return {
			onValidateButtonClick: () => {
				const panel = $("#client-grant-panel");

				const clientId = $.trim($("#clientId").val());
				const resourceAri = $.trim($("#resourceAri").val());
				const scopes = $.trim($("#scopes").val()).split(",").map(scope => scope.trim());
				const audience = $("#audience").val();

				const validationRequest = {"resourceAri" : resourceAri, "scopes": scopes, "audience": audience};

				const loading = panel.find(".loading");
				const button = panel.find("#validate-button")
				loadingState(loading, button, true);

				$.when(
					oauthManagmentService(service).validateClientGrants(clientId, validationRequest)
				).done((validationResult) => {
					loadingState(loading, button, false);
					panel.find("#validation-result").html(JSON.stringify(validationResult));
					panel.find(".results").show();
				}).fail(error => {
					switch(error.status) {
						case 400:
							AJS.flag(errorFlags.badRequest); break;
						case 404:
							AJS.flag(errorFlags.notFound); break;
						default:
							AJS.flag(errorFlags.serverError); break;
					}
					loadingState(loading, button, false);
				});
			},

			onInstallButtonClick: () => {
				const panel = $("#client-grant-panel");

				const clientId = $.trim($("#clientId").val());
				const resourceAri = $.trim($("#resourceAri").val());
				const scopes = $.trim($("#scopes").val()).split(",").map(scope => scope.trim());
				const audience = $("#audience").val();

				const installRequest = {"oauthClientId": clientId, "resourceAri" : resourceAri, "scopes": scopes, "audience": audience};

				const loading = panel.find(".loading");
				const button = panel.find("#install-button")
				loadingState(loading, button, true);

				$.when(
					oauthManagmentService(service).manageClientGrants(installRequest)
				).done((installResult) => {
					loadingState(loading, button, false);
					AJS.flag(successFlags.installSuccess);
					panel.find("#install-result").html(JSON.stringify(installResult));
					panel.find(".results").show();
				}).fail(error => {
					switch(error.status) {
						case 400:
							AJS.flag(errorFlags.badRequest); break;
						case 404:
							AJS.flag(errorFlags.notFound); break;
						default:
							AJS.flag(errorFlags.serverError); break;
					}
					loadingState(loading, button, false);
				});
			}

		};
	})();

	$("#validate-button").click(panelPresenter.onValidateButtonClick);
	$("#install-button").click(panelPresenter.onInstallButtonClick);

	window.panelPresenter = panelPresenter;
});
